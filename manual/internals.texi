@node Internals
@chapter Internals

@menu
* Structure::		Structure of a reliable archive
* Procedure::		Procedure followed by @command{ra-create}
@end menu

@node Structure
@section Structure of a reliable archive
@cindex structure of archive
@cindex archive structure

A reliable archive is simply a set of files in a directory. There are several
types of files which comprise an archive.

@menu
* rarecord file::
* Data files::
* Parity files::
@end menu

@node rarecord file
@subsection The @file{rarecord} file
@cindex @file{rarecord} file

The @file{rarecord} file stores a description of the archive, with sufficient
information so that @command{ra-restore} can read it back and verify the
archive's integrity.  @file{rarecord} files consist of two main sections, the
configuration block and the file listing.

When an archive is distributed across multiple discs, each disc contains a
copy of the @command{rarecord} file.

The default name for the @file{rarecord} file is @file{rarecord}; if this is
changed, then any command operating on an archive will need to be informed of
the new name.

@menu
* Configuration block::
* File listing::
@end menu

@node Configuration block
@subsubsection Configuration block
@cindex Configuration block, @file{rarecord}

The first section of the rarecord file is the configuration block - a block of
data containing the parameters with which the archive was originally created.
It is essentially a dump of the internal configuration variables set by the
options to @command{ra-create}.  It allows @command{ra-restore} to know how an
archive was assembled, and allows @command{ra-create} to go back and update an
archive without re-specifying all parameters.

@node File listing
@subsubsection File listing
@cindex File listing, @file{rarecord}

After the configuration block is the file listing - a list of all files (and
chunks and parity files) contained in the archive.  It provides the checksum
for each file or chunk, as well as the type of the file (file, split file,
file chunk, or parity chunk).

@node Data files
@subsection Data files
@cindex Data files

The actual data contained in the archive is stored in the data files.  If a
data file is copied to the archive unaltered (that is, it is smaller than the
requested hunk size and does not need to be split), it appears in the archive
with its original name.

Split data files appear with their original names suffixed with a split part
identifier; this is, by default, a sequence of 5 lowercase letters.  See the
documentation for your system's split(1) command for details.

@node Parity files
@subsection Parity files
@cindex Parity files

The parity data is stored in a set of parity files in the archive.

When an archive is distributed across discs, each parity file will appear on
at least two different discs.

@node Procedure
@section Procedure
@cindex Theory of operation

The relarch system is fairly simple.  This section will describe what all
goes on under the hood when the @command{ra-create} command is run.

@menu
* Step 1: File splitting and checksumming
* Step 2: Parity data creation
* Step 3: Disk distribution
@end menu

@node File splitting and checksumming
@subsection File splitting and checksumming
@cindex splitting
@cindex checksum

The first operation relarch does is to split and checksum the files.  An MD5
checksum is computed for each file that will be in the final archive, and this
is stored in the @file{rarecord} file (@pxref{rarecord file}).

Files that are larger than the desired chunk size are then split, using the
chunk size as a maximum file size.  A checksum of each chunk is created, and
stored.

@node Parity data creation
@subsection Parity data creation
@cindex parity

After @command{ra-create} has split and checksummed the data files, it creates
the parity data.  The @command{par2} program is run over the entire data set,
and it produces a series of parity files in the archive.  These files are also
checksummed (so corrupt ones can be avoided), and recorded in the
@file{rarecord} file (@pxref{rarecord file}).

@node Disk distribution
@subsection Disk distribution
@cindex distribution

Unless it is called with the @option{-n} option, @command{ra-create} will
distribute the archive's files into directories for CD burning after it has
created the archive.

@menu
* Distribution Algorithm::	The procedure used for distributing files
@end menu

@node Distribution Algorithm
@subsubsection Distribution algorithm
@cindex Distribution Algorithm

The distribution algorithm begins by computing the total space needed by the
archive, how many discs it will require, and creating the directories needed.
It then proceeds as follows:

@enumerate
@item Place the @file{rarecord} file on each disc.

@item Distribute the parity data files.  For each file, pick a random disc i to
place it in; then place another copy in disc @math{(i + n/2) {\rm mod} n},
where @math{n} is the number of discs.  For example, if you have a 5-disc set,
and a parity file is placed on disc 2, it will also place that file on disc 4.

@item Distribute the data files.  This is done by picking the largest
remaining data file, and placing it on the disc with the most free space,
until no more files remain.
@end enumerate

All files are placed, if possible, as hard links to the files in the archive
output directory to save temporary file space.
